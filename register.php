<?php
$gender = array(0 => "Nam", 1 => "Nữ");
$faculty = array("" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");
?>

<html>

<head>
    <meta charset="UTF-8">
    <title>day03</title>
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <div class="container">
        <form class="form-wrapper">
            <div>
                <label for="name" class="form-label">Họ và tên</label>
                <input type="text" name="name" id="name" class="form-input" />
            </div>
            <div>
                <label for="gender" class="form-label">Giới tính</label>
                <?php
                for ($i = 0; $i < 2; $i++) {
                    echo "<input type='radio' id={$i} name='gender' value={$i} >
                         <label for={$i}>{$gender[$i]}</label>";
                }
                ?>
            </div>
            <div class="select-wrapper">
                <label for=" faculty" class="form-label">Phân khoa</label>
                <span class="select-arrow"></span>
                <select name=" faculty" id="faculty" class="form-select">
                    <?php foreach ($faculty as $key => $value) : ?>
                        <option value=<?= $key; ?>><?= $value; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <input type="submit" value="Đăng ký" class="submit-btn" />
        </form>
    </div>
</body>

</html>
